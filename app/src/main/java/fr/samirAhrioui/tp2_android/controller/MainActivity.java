package fr.samirAhrioui.tp2_android.controller;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import fr.samirAhrioui.tp2_android.R;
import fr.samirAhrioui.tp2_android.model.Book;
import fr.samirAhrioui.tp2_android.model.BookDbHelper;
import fr.samirAhrioui.tp2_android.view.MyCursorAdapter;

public class MainActivity extends AppCompatActivity {

    private ListView mListView ;
    private FloatingActionButton fab;
    private BookDbHelper mBookDbHelper;
    MyCursorAdapter myCursorAdapter;
    private Cursor mCursor;
    public static final String SELECTED_BOOK = "SELECTED_BOOK";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        System.out.println("MainActivity :: onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        

        mBookDbHelper = new BookDbHelper(this);
       // mBookDbHelper.populate();
        mListView = findViewById(R.id.list_book);

        mCursor = mBookDbHelper.fetchAllBooks();

        myCursorAdapter = new MyCursorAdapter(this, mCursor,1);
        mListView.setAdapter(myCursorAdapter);


        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mCursor.moveToPosition(position);
                Book selectedBook = mBookDbHelper.cursorToBook(mCursor);
                Intent intent = new Intent(MainActivity.this,BookActivity.class);
                intent.putExtra(SELECTED_BOOK, selectedBook);
                startActivity(intent);

            }
        });
        // we register for the contextmneu
        registerForContextMenu(mListView);

        mListView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            // We want to create a context Menu when the user long click on an item
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v,
                                            ContextMenu.ContextMenuInfo menuInfo) {
                menu.add(Menu.NONE, 1, Menu.NONE, "Supprimer");
            }



        });

        fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this,BookActivity.class);
                startActivity(intent);
            }
        });


    }
    // This method is called when user selects an Item in the Context menu
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        mCursor.moveToPosition(info.position);
        mBookDbHelper.deleteBook(mCursor);
        mCursor = mBookDbHelper.fetchAllBooks();
        myCursorAdapter.changeCursor(mCursor);
        myCursorAdapter.notifyDataSetChanged();
        return true;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        System.out.println("MainActivity :: onStart()");

    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("MainActivity :: onResume()");
        mCursor = mBookDbHelper.fetchAllBooks();
        myCursorAdapter.changeCursor(mCursor);
        myCursorAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onPause() {
        super.onPause();
        System.out.println("MainActivity :: onPause()");
    }

    @Override
    protected void onStop() {
        super.onStop();
        System.out.println("MainActivity :: onStop()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.out.println("MainActivity :: onDestroy()");
    }
}
